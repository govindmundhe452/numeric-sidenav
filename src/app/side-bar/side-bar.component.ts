import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
  animations : [
    trigger('flyInOut', [
      state('0', style({ transform : "translateX(-150%)", opacity : 0, width:0 })),
      transition('* => *', animate('400ms ease-in-out')),
    ])
  ]
})
export class SideBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  currentState = '1';
  changeState() {
    this.currentState = this.currentState === '1' ? '0' : '1';
  }
}
